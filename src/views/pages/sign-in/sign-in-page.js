import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { authActions } from 'src/auth';
import Button from 'src/views/components/button';
import '../../../../node_modules/font-awesome/css/font-awesome.min.css'; 

import './sign-in-page.css';


const SignInPage = ({signInWithGoogle, signInWithFacebook}) => {
  return (
    <div className="g-row sign-in">

      <div className="g-row signin_with_providers">
        <div className="g-col">
          <p className="sign-in__heading">Sign in</p>
          <Button className="sign-in__button" onClick={signInWithGoogle}> 
            <i className="fa fa-google" aria-hidden="true"></i>
          </Button>
          <Button className="sign-in__button" onClick={signInWithFacebook}>
              <i className="fa fa-facebook" aria-hidden="true"></i>
          </Button>
        </div>
      </div>

      <div className="g-row signin_with_email">
      </div>

      <div className="g-row register">
        <div className="g-col">

        </div>
      </div>

    </div>
  );
};

SignInPage.propTypes = {
  signInWithGoogle: PropTypes.func.isRequired,
  signInWithFacebook: PropTypes.func.isRequired
};


//=====================================
//  CONNECT
//-------------------------------------

const mapDispatchToProps = {
  signInWithGoogle: authActions.signInWithGoogle,
  signInWithFacebook: authActions.signInWithFacebook
};

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(SignInPage)
);
