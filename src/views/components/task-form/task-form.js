import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from '../icon';
import Button from '../button';
import classNames from 'classnames';

import './task-form.css';


export class TaskForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired
  };

  constructor() {
    super(...arguments);

    this.state = {title: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    // this.setfocus = this.setfocus.bind(this);
  }

  // setfocus() {
  //   input.task-form__input.focus();
  // }

  clearInput() {
    this.setState({title: ''});
  }

  handleChange(event) {
    this.setState({title: event.target.value});
  }

  handleKeyUp(event) {
    if (event.keyCode === 27) this.clearInput();
  }

  handleSubmit(event) {
    event.preventDefault();
    const title = this.state.title.trim();
    if (title.length) this.props.handleSubmit(title);
    this.clearInput();
  }

  render() {
    return (
      <form className="task-form" onSubmit={this.handleSubmit} noValidate>

        <div className="addbutton">
          <Button
            className={classNames('btn--icon', 'task-item__button')}
            // onClick={this.setfocus}            
            >
            <Icon name="add" />
          </Button>
        </div>

        <div className="newtaskinput" >
          <input
            autoComplete="off"
            autoFocus
            className="task-form__input"
            onChange={this.handleChange}
            onKeyUp={this.handleKeyUp}
            onBlur={this.handleSubmit}
            placeholder=""
            ref={e => this.titleInput = e}
            type="text"
            value={this.state.title}
          />
        </div>

        <div className="sendbutton" >
          <Button
            className={classNames('btn--icon', 'task-item__button')}>
            <Icon name="send" />
          </Button>
        </div>

      </form>
    );
  }
}


export default TaskForm;
